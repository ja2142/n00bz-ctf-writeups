#!/usr/bin/env python3
# n00bz{0k4y_h34r_m3_out_c0m1ng_up_w1th_fl4gs_1snt_4lw4ys_34sy_3sp3c14lly_l0ng_fl4gs_4nyw4y_y0u_4r3_d01ng_4w3s0m3_k33p_1t_up}

from pwn import *
from enum import Enum

class DecrResult(Enum):
    BETTER = 0
    WORSE = 1
    SAME = 2

def play(target, ct):
    target.sendlineafter(b'>> ', b'p')
    target.sendlineafter(b'x: ', str(ct).encode())
    target.recvline()
    result_line = target.recvline().decode()
    log.info(f'result: {result_line}')
    if 'Warmer' in result_line:
        return DecrResult.BETTER
    elif 'Colder' in result_line:
        return DecrResult.WORSE
    else:
        return DecrResult.SAME

target = remote('159.65.232.9', 10334)

target.recvuntil(b'N: ')
N = int(target.recvline(keepends=False).decode())
log.info(f'{N=}')

target.sendlineafter(b'x: ', b'1')

target.recvline()
encrypted_one = target.recvline()
encrypted_one = int(encrypted_one[1:])
print(f'enc(1)={encrypted_one}')

pt = 0
current_bit_enc = encrypted_one
current_bit = 1

for i in range(1024):
    # starting from bit 1, instead of 0, starting from 0 would require some more fuckery
    # bit 0 can be guessed later
    current_bit *= 2
    current_bit_enc = pow(current_bit_enc, 2, N*N)
    log.info(f'current bit: {current_bit:x}')

    # next play will compare to 1
    play(target, encrypted_one)
    # compare 1 | tested_bit
    to_comp = (encrypted_one * current_bit_enc) % (N*N)
    result = play(target, to_comp)
    if result == DecrResult.BETTER:
        pt += current_bit

    log.info(f'{pt=:x}')
    log.info(pt.to_bytes(256, 'big').decode())

print(f'{pt=:x}')
print(pt.to_bytes(256, 'big').decode())
