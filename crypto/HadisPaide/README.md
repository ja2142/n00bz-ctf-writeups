# HadisPaide

## HADIS

https://www.alabamapublichealth.gov/hiv/hadis.html

HADIS, the HIV/AIDS Disease Information System, is a patient-centric system...

## Ok let me start over

Application working with Paillier cryptosystem allows two operations:
 - encrypting a chosen message (only once),
 - decrypting a chosen ciphertext, and outputting if number of correct bits is greater or lesser then last checked ciphertext.

## #nohomomorphism

In Paillier decrypting product of ciphertexts gives sum of plaintexts:

```
pt0 = dec(ct0)
pt1 = dec(ct1)
dec(ct0 * ct1) = pt0 + pt1
```

## Solved, basically

By encrypting "1", it's possible to generate ciphertext of any other message by raising it to that power:

```
dec(encrypted_one * encrypted_one) = 2
dec(encrypted_one ^ n) = n
```

From that it's possible to test flag bit by bit, by testing if adding 2^n to 1 takes decrypted message closer to flag.
