# RSA-OOPS

## Weird RSA based system

Application let's user encrypt flag with "RSA-OOPS" a limited amount of times with the same key.

The system seems to be an attempt at encrypting message with secret random padding value at once by dividing keys and ciphertexts into two parts:

```
X = (m+pool_random)^eL (mod nL) # message "padded" with pool random
Y = X^dR (mod nR) xor pool_random # padding encrypted with separate key
```

## Size matters

Size of nR key (~200 bits) makes it possible to factor nR in a matter of seconds, retrieve dR, and calculate pool_random.

That leaves attacker with an ability to create messages in form of:
```
ct[x] = (flag+b[x])^e (mod n)
```
where b[x] (aka pool_random) is a random, but known value.

One way of retrieving encrypted message from that (having e ciphertexts, where e is the exponent (17 in this case)):
https://www.iacr.org/archive/pkc2005/33860001/33860001.pdf

## Solution

All that needs to be done is:
 - get nR and factor it,
 - gather 17 ciphertexts,
 - retrieve pool_random for each ciphertext,
 - run "algorithm 1" from paper above.

## Alternatives

There's at least one alternative way of retrieving flag from series of related ciphertexts:
https://link.springer.com/content/pdf/10.1007%2F3-540-68339-9_1.pdf
seems to allow to recover linearly related messages from just two ciphertexts (but the paper is more general, so it probably requires a bit more understanding to implement this attack).
